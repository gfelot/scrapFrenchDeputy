const Crawler = require("crawler");
const scrapID = require('./scrapID');
const scrapFiche = require('./scrapFiche');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const _ = require('lodash')
const inquirer = require('inquirer');

console.log('Get ready to parse some french deputy !!!!!');

const questions = [
    {
        type: 'list',
        name: 'groupe',
        message: 'Pour quel groupe parlementaire veux tu scapper les données',
        choices: ['LR', 'REM', 'NI', 'LC', 'FI', 'NG', 'GDR', 'MODEM']
    }
]


inquirer.prompt(questions).then(function (answers) {


    console.log('\n\t\t💀 💀 💀 💀 💀 💀 💀 💀 💀 💀');
    console.log('\t\t💀 💀 💀  Start  💀 💀 💀');
    console.log('\t\t💀 💀 💀 💀 💀 💀 💀 💀 💀 💀\n');
    
    
    const getId = {
        uri: 'http://www2.assemblee-nationale.fr/deputes/recherche-multicritere',
        method: 'POST',
        form: {
            'infosGenFiche':'fiche',
            'infosGenCiv':'default',
            'infosGenPrenom':'default',
            'infosGenNom':'default',
            'infosGenAge':'default',
            'regroup':'default',
            'regroup2':'default',
            'critSexe':'default',
            'critGroupe':`${answers.groupe}`,
            'critComper':'default',
            'critFamilleSociopro':'default',
            'critCatSociopro':'default',
            'critDept':'default',
            'critRegion':'default',
            'critMandatCommunal':'default',
            'critMandatDept':'default',
            'critMandatRegional':'default',
        }
    }
    
    let IDs = [];
    let urlInfo = []
    let fichesPerso = []
    
    function makeURL(subURL, i) {
        const option = {
            uri: `http://www2.assemblee-nationale.fr${subURL}`,
            // uri: 'http://www2.assemblee-nationale.fr/deputes/fiche/OMC_PA719550',
            method: 'GET'
        }
    
        urlInfo.push(option);
    }
    
    const c1 = new Crawler({
        callback: function(err, res, done) {
            if (err) { console.log(err) }
            else {
                const $ = res.$;
                IDs = scrapID($);
                IDs.forEach(makeURL);
                done();
                

                c2.on('drain', () => {
                    // console.log(output);
                    console.log(`${__dirname}/${answers.groupe}_${moment().format('MM_DD_HH:mm:ss')}.json`)
                    fs.writeFile(`${__dirname}/${answers.groupe}_${moment().format('MM_DD_HH:mm:ss')}.json`, JSON.stringify(fichesPerso), err => {
                      if (err) {
                        console.error(err);
                      }
                      console.log('💀 💀 💀 Final Json Done ! 💀 💀 💀');
                    })
                  })


                c2.queue(urlInfo);
    
            }
        }
    })
    
    const c2 = new Crawler({
        callback: function(err, res, done) {
            if (err) { console.log(err) }
            else {
                const $ = res.$;
                fichesPerso.push(scrapFiche($, answers.groupe));
                done();
            }
        }
    })
    
        
    c1.queue(getId);
    
  });

