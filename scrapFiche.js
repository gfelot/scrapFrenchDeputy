const cheerio = require('cheerio');
const _ = require('lodash')


module.exports = function($, group) {
    console.log('Get info by deputée');

    let perso = {}
    var obj = $(this)
    

    const imgPath = $('.deputes-image').children('img').attr('src')
    const imgName = _.replace(imgPath, '/static/tribun/15/photos/', '')
    perso._id = _.replace(imgName, '.jpg', '');
    perso.groupe = group
    perso.name = $('.titre-bandeau-bleu').children('h1').text()
    perso.circoncription = $('.titre-bandeau-bleu').children('h1').next().text()
    perso.enCours = $('.titre-bandeau-bleu').children('.deputy-healine-sub-title.orange').text()
    perso.suppleant = $('.deputes-liste-attributs').children('dd').eq(2).children('ul').children('li').text()
    perso.siteWeb = $('.vcard .url').attr('href')
    const emailToClean1 = $('.vcard .email').eq(0).attr('href');
    perso.emailAssemblee = _.replace(emailToClean1, 'mailto:', '');
    const emailToClean2 = $('.vcard .email').eq(1).attr('href');
    perso.emailPerso = _.replace(emailToClean1, 'mailto:', '');
    perso.facebook = $('.vcard .facebook').attr('href');
    perso.twitter = $('.vcard .twitter').attr('href');
    const placeName = $('.vcard .adr .street-address').eq(2).text()
    const placeStreet = $('.vcard .adr .street-address').eq(3).text()
    const placeZip = $('.vcard .adr .postal-code').eq(1).text()
    const placeLocality = $('.vcard .adr .locality').eq(1).text()
    perso.address = `${placeName} | ${placeStreet} | ${placeZip} | ${placeLocality}`
    perso.tel = $('.vcard .tel span').text()
    perso.collabarateur = [];
    const listSupleant = $('#deputes-place').children('.interieur-contenu-secondaire').children('.plusieurs-element-simple').children('.corps-contenu').children('ul').children('li')
    listSupleant.each(function (i, sup) {
       perso.collabarateur.push($(sup).text())
    })

    return perso;
}